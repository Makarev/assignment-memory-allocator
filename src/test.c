#include "test.h"

void log_start(size_t number) {
    printf("Start test - %zu\n", number);
}

void log_end(size_t number) {
    printf("End test - %zu\n", number);
}



void init() {
    heap_init(HEAP_SIZE);
}

void test1(){
    log_start(FIRST);
    init();
    void * mem = _malloc(FIRST_ALLOC);
    debug_heap(stdout, HEAP_START);
    _free(mem);
    log_end(FIRST);
}

void test2(){
    log_start(SECOND);
    void * mem1 = _malloc(FIRST_ALLOC);
    void * mem2 = _malloc(SECOND_ALLOC);
    debug_heap(stdout, HEAP_START);
    _free(mem1);
    _free(mem2);
    log_end(SECOND);
}


